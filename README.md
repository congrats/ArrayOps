#ArrayOps
------

###What's it?
It provides a simple framework of Java's array operatoins. </br>
You can easily customize it through relevant interface. </br>

###For waht?
Actually, I didn't find a very convenience and pretty interface for array's operation in JDK, such as add, substract, filt. You might think of the `Arrays` utils, but it still can not satify above requirment. So I create it for my daily work and share it for people that meet the same requirements.

###How to use
Coding like this:
```
import static net.haibo.utils.ArrayOps.*;


String s = "no news is a good news";
List<String> the = new ArrayList<String>(Arrays.asList(s.split("[\\s]")));
System.out.println(the);

List<String> filt = arrayBuilder(the).filt(sMatchWith("^.*s.*$")).build();
List<String> sub = arrayBuilder(the).subtract(filt).build();
List<String> add = arrayBuilder(sub).add(filt).build();
System.out.println(filt); System.out.println(sub); System.out.println(add);

```

</br>
OUTPUT:
```
[no, news, is, a, good, news]
[news, is, news]
[no, a, good]
[no, a, good, news, is, news]
```